import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule, HttpClient } from '@angular/common/http'

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DataDrivenLoginPageModule } from '../pages/data-driven-login/data-driven-login.module';
import { TemplateDrivenLoginPageModule } from '../pages/template-driven-login/template-driven-login.module';
import { ConectDbProvider } from '../providers/conect-db/conect-db';
import { LoginProvider } from '../providers/login/login';



@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    DataDrivenLoginPageModule,
    TemplateDrivenLoginPageModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConectDbProvider,
    LoginProvider
  ]
})
export class AppModule {}
