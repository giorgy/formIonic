import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataDrivenLoginPage } from './data-driven-login';

@NgModule({
  declarations: [
    DataDrivenLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(DataDrivenLoginPage),
  ],
})
export class DataDrivenLoginPageModule {}
