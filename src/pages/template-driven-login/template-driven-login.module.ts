import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TemplateDrivenLoginPage } from './template-driven-login';

@NgModule({
  declarations: [
    TemplateDrivenLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(TemplateDrivenLoginPage),
  ],
})
export class TemplateDrivenLoginPageModule {}
