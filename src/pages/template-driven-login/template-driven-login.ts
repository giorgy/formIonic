import { Component, ViewChild, Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LoginProvider } from '../../providers/login/login';


@IonicPage()
@Component({
  selector: 'page-template-driven-login',
  templateUrl: 'template-driven-login.html',
})




export class TemplateDrivenLoginPage {
  @ViewChild('formUser') formUser:FormGroup
  testUser = new TestLoginUser

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public http:HttpClient,
    public login: LoginProvider,
    private loading: LoadingController,
    
    //public testLoginUser: TestLoginUser
  ) 

  {}

  protected loginForm(formLogin:FormGroup) {
    let load = this.loading.create({ content: "loading..." })
    load.present()
    if (formLogin.valid){
      this.login.loginUser(formLogin.value).subscribe(
        sucesso =>{
          this.toast("sucesso" + sucesso)
          load.dismiss()
        }, error =>{
          this.toast("Error" + error)
          load.dismiss()
        }
      )
    } else{
      this.toast('Email and Password Invalid')
    }
    console.log(formLogin.value)
  }

protected toast(message){
  this.toastCtrl.create({
    message:message,
    duration:3000,
    position: 'bottom'
  }).present()
}

ionViewDidLoad(){
  
  console.log(this.testUser)
  }
}


export class TestLoginUser {
  email: string
  password: string

  constructor() {
    this.email = 'giorgyismael@gmail'
    this.password = '123456'
  }
}
